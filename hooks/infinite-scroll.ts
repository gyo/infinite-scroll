import React from "react";
import { InfiniteScrollKeys } from "../constants/infinite-scroll";
import { useInfiniteScrollContext } from "../contexts/infinite-scroll";
import { Member } from "../model/member";
import { User } from "../model/user";

type Props = {
  /**
   * key
   *
   * グローバルな React Context に保存している Infinite Scroll のデータのキー。
   * 各 Infinite Scroll ごとに固有の値になる。
   */
  key: InfiniteScrollKeys;

  intersectionObserverOptions: {
    rootMargin: string;
  };
  buildApiPath: (page: number) => string;
  onLoad: () => void;
  onError: (error: Error) => void;
};

export const useInfiniteScroll = (props: Props) => {
  /**
   * infiniteScrollState, setInfiniteScrollState
   *
   * グローバルな React Context からデータを取得する。
   */
  const [
    infiniteScrollState,
    setInfiniteScrollState,
  ] = useInfiniteScrollContext(props.key);

  /**
   * loading, setLoading, finished, setFinished
   *
   * ロード中や、全ての要素を読み込み済み、といった状態を表すためのフラグ。
   */
  const [loading, setLoading] = React.useState(false);
  const [finished, setFinished] = React.useState(false);

  /**
   * sentinelIntersectionRatio
   *
   * sentinelIntersectionRatio は undefined で初期化し、
   * IntersectionObserver のコールバックが発火するたびに intersectionRatio を代入するようにする。
   *
   * IntersectionObserver のコールバックが発火する際には
   * entry.isIntersecting && sentinelIntersectionRatio.current == 0
   * でチェックを行うことにより、sentinel が領域外から領域内へ移動したときのみ要素の読み込みを行うようにする。
   *
   * これにより、
   * - API がなんらかのエラーで要素を0件返す際の無限発火の防止 *1、
   * - Chrome のスクロールアンカリングに起因する無限発火の防止 *2、
   * - 初回描画時に sentinel がページ内に存在する場合の要素の読み込みの防止 *3
   * などの効果を得られる。
   *
   * *1
   * これは finished フラグでも制御している。
   *
   * *2
   * スクロールアンカリングを防止するには、CSS の overflow-anchor: none; をつかったり、
   * 一覧への要素追加時に要素を追加するトリガーとなる要素を非表示（三項演算子で描画しない）にするなどの方法がある。
   *
   * *3
   * 初回描画時には initialLoadFinished フラグを用いて1ページ目を読み込んでいる。
   */
  const sentinelIntersectionRatio = React.useRef<number>();

  /**
   * load
   *
   * API から要素を読み込み state を更新する。
   *
   * loading フラグを用いて API の結果を待っている間に再度発火しないようにしている。
   * 依存に list.items, list.page が指定されているため、
   * load が発火して state が更新される前に再度 load が発火しても
   * 同じ要素が繰り返し append された一覧になることはない。
   */
  const load = React.useCallback(() => {
    (async () => {
      if (loading || finished) {
        return;
      }

      setLoading(true);

      const nextPage = infiniteScrollState.page + 1;

      try {
        const response = await fetch(props.buildApiPath(nextPage));

        if (response.status < 400) {
          const nextItems = await response.json();
          setInfiniteScrollState({
            items: [...infiniteScrollState.items, ...nextItems],
            page: nextPage,
          });
          props.onLoad();

          if (nextItems.length === 0) {
            setFinished(true);
          }
        } else {
          props.onError(new Error("読み込みに失敗しました。"));
        }
      } catch (error) {
        console.error(error);
        props.onError(error);
      }

      setLoading(false);
    })();
  }, [
    finished,
    infiniteScrollState.items,
    infiniteScrollState.page,
    loading,
    props,
    setInfiniteScrollState,
  ]);

  /**
   * sentinelRef, useEffect
   *
   * Infinite Scroll のための IntersectionObserver を登録する。
   *
   * sentinelRef はこのカスタムフックの初期化時に返り値として呼び出し側へ渡される。
   * 呼び出し側はこの sentinelRef を IntersectionObserver の sentinel として利用する Element へ紐づける。
   *
   * 例：
   * ```
   * const { sentinelRef } = useInfiniteScroll()
   * <div ref={sentinelRef}></div>
   * ```
   */
  const sentinelRef = React.useRef<HTMLDivElement>(null);
  React.useEffect(() => {
    const sentinelElement = sentinelRef.current;

    const observer = new IntersectionObserver(
      (entries) => {
        const entry = entries[0];

        if (entry.isIntersecting && sentinelIntersectionRatio.current == 0) {
          load();
        }

        sentinelIntersectionRatio.current = entry.intersectionRatio;
      },
      {
        rootMargin: props.intersectionObserverOptions.rootMargin,
      }
    );

    if (sentinelElement != null) {
      observer.observe(sentinelElement);
    }

    return () => {
      if (sentinelElement != null) {
        observer.unobserve(sentinelElement);
      }
    };
  });

  /**
   * initialLoadFinished
   *
   * 描画後に1ページ目を読み込む際に使用するフラグ。
   *
   * 一覧の要素をグローバルな React Context に保存することにしたため、
   * getServerSideProps などを使って1ページ目を読み込むことができない。
   * ページ描画時に一覧の要素が0件の場合は load を発火させる。
   * ただし、state が更新されるたびに load が発火するのを防ぐため、フラグで実行回数を制御している。
   */
  const initialLoadFinished = React.useRef(false);
  if (
    infiniteScrollState.items.length === 0 &&
    initialLoadFinished.current === false
  ) {
    load();
    initialLoadFinished.current = true;
  }

  return {
    items: infiniteScrollState.items,
    loading,
    finished,
    sentinelRef,
    load,
  };
};
