import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Error from "next/error";
import React from "react";
import { Links } from "../../components/Links";
import { members } from "../../constants/members";
import { Member } from "../../model/member";

type Props = {
  item?: Member;
};

export const getStaticProps: GetStaticProps<Props> = async (context) => {
  const props: Props = {};

  const item = members.find((item) => {
    return item.id === context.params?.id;
  });
  if (item != null) {
    props.item = item;
  }

  return {
    props,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [
      ...members.map((item) => {
        return {
          params: { id: item.id },
        };
      }),
    ],
    fallback: false,
  };
};

const Page: NextPage<Props> = (props) => {
  if (props.item == null) {
    return <Error statusCode={500} />;
  }

  return (
    <>
      <Links h1={`Member ${props.item.id}`} />
      <ul>
        <li>{props.item.index}</li>
        <li>{props.item.id}</li>
        <li>{props.item.name}</li>
        <li>{props.item.tel}</li>
        <li>{props.item.email}</li>
      </ul>

      <style jsx>{`
        ul {
          margin-top: 64px;
        }
      `}</style>
    </>
  );
};

export default Page;
