import { NextPage } from "next";
import NextLink from "next/link";
import React from "react";
import { Links } from "../../components/Links";
import { INFINITE_SCROLL_KEY } from "../../constants/infinite-scroll";
import { useInfiniteScroll } from "../../hooks/infinite-scroll";
import { Member } from "../../model/member";

const buildApiPath = (page: number) => {
  if (process.env.NODE_ENV === "production") {
    return `https://infinite-scroll-dun.vercel.app/api/members?page=${page}`;
  } else {
    return `http://localhost:3000/api/members?page=${page}`;
  }
};

const Page: NextPage = () => {
  /**
   * timerId, errorMessage, setErrorMessage
   *
   * Infinite Scroll とは無関係。
   * 読み込みに失敗した際のエラーメッセージの表示非表示に使う。
   */
  const timerId = React.useRef<NodeJS.Timeout>();
  const [errorMessage, setErrorMessage] = React.useState<string>();

  /**
   * infiniteScroll
   *
   * Infinite Scroll を実現するためのカスタムフック useInfiniteScroll の返り値
   */
  const infiniteScroll = useInfiniteScroll({
    key: INFINITE_SCROLL_KEY.MEMBERS,
    intersectionObserverOptions: {
      rootMargin: "320px",
    },
    buildApiPath,
    onLoad: () => {
      setErrorMessage(undefined);
    },
    onError: (error: Error) => {
      if (timerId.current != null) {
        clearTimeout(timerId.current);
      }
      setErrorMessage(error.message);
      timerId.current = setTimeout(() => {
        setErrorMessage(undefined);
      }, 2000);
    },
  });

  return (
    <>
      <Links h1="Members" />
      <ul>
        {/* TODO: as を使わない形へ修正する */}
        {(infiniteScroll.items as Member[]).map((item, index) => {
          const href = `/members/${item.id}`;
          return (
            <li key={`${item.id}-${index}`}>
              <NextLink href={href}>
                <a>
                  <PageCard member={item} />
                </a>
              </NextLink>
            </li>
          );
        })}
      </ul>

      <div ref={infiniteScroll.sentinelRef}></div>

      {/* 要素を追加する際には、infiniteScroll.loading を用いて、要素を追加するトリガーとなった要素は描画されないようにする。 */}
      {/* これをしないと、スクロールアンカリングの効果によって、トリガーとなった要素が画面内に表示されるようにスクロール位置がコントロールされ、 */}
      {/* ユーザーが追加された要素を認識できなくなってしまう。 */}
      {/* CSS の overflow-anchor: none; でも制御可能。 */}
      <div className="load-more">
        {!infiniteScroll.finished ? (
          infiniteScroll.loading ? (
            <span>読み込み中...</span>
          ) : (
            <button onClick={infiniteScroll.load}>さらに読み込む</button>
          )
        ) : (
          <span>すべての要素を読み込みました。</span>
        )}
      </div>

      {errorMessage != null ? (
        <div className="error">{errorMessage}</div>
      ) : null}

      <style jsx>{`
        ul {
          list-style: none;
          margin: 0;
          padding: 32px;
        }
        li {
          margin-top: 8px;
        }
        a {
          color: inherit;
          text-decoration: inherit;
        }
        .load-more {
          display: grid;
          place-items: center;
          padding-bottom: 32px;
        }
        .error {
          position: fixed;
          right: 0;
          bottom: 0;
          padding: 4px;
          font-size: 12px;
          background: crimson;
          color: white;
        }
      `}</style>
    </>
  );
};

export default Page;

const PageCard: React.FC<{ member: Member }> = (props) => {
  return (
    <>
      <div className="card">
        <p>
          {props.member.index} {props.member.name}
        </p>
      </div>

      <style jsx>{`
        .card {
          padding: 32px;
          background: ghostwhite;
        }
      `}</style>
    </>
  );
};
