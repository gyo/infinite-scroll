import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Error from "next/error";
import Image from "next/image";
import React from "react";
import { Links } from "../../components/Links";
import { users } from "../../constants/users";
import { User } from "../../model/user";

type Props = {
  item?: User;
};

export const getStaticProps: GetStaticProps<Props> = async (context) => {
  const props: Props = {};

  const item = users.find((item) => {
    return item.id === context.params?.id;
  });
  if (item != null) {
    props.item = item;
  }

  return {
    props,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [
      ...users.map((item) => {
        return {
          params: { id: item.id },
        };
      }),
    ],
    fallback: false,
  };
};

const Page: NextPage<Props> = (props) => {
  if (props.item == null) {
    return <Error statusCode={500} />;
  }

  return (
    <>
      <Links h1={`User ${props.item.id}`} />
      <div className="card">
        <div className="card__image">
          <Image
            width="100"
            height="100"
            src={`https://robohash.org/${props.item.id}.png?set=set4`}
            alt="アバター画像"
          />
        </div>
        <div className="card__title">{props.item.name}</div>
        <div className="card__body">{props.item.message}</div>
      </div>

      <style jsx>{`
        .card {
          display: grid;
          grid-template: auto auto / auto 1fr;
          gap: 16px 32px;
          margin-top: 64px;
          padding: 32px;
          background: ghostwhite;
        }
        .card__image {
          grid-area: 1 / 1 / 2 / 2;
          width: 96px;
          height: 96px;
          border-radius: 50%;
          background: white;
          overflow: hidden;
        }
        .card__title {
          grid-area: 1 / 2 / 2 / 3;
          align-self: center;
          font-size: 40px;
          font-weight: bold;
        }
        .card__body {
          grid-area: 2 / 1 / 3 / 3;
          font-size: 32px;
        }
      `}</style>
    </>
  );
};

export default Page;
