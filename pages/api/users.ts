import { NextApiRequest, NextApiResponse } from "next";
import { users } from "../../constants/users";
import { User } from "../../model/user";

const SLEEP_MS = 500;
const ITEMS_PER_PAGE = 10;
const FAILURE_RATE = 0;

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  await sleep(SLEEP_MS);

  if (willFail()) {
    res.statusCode = 400;
    res.end();
    return;
  }

  const page = buildPageNumber(req);
  const nextItems = sliceItems(users, page);

  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");
  res.end(JSON.stringify(nextItems));
};

export default handler;

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const buildPageNumber = (req: NextApiRequest) => {
  let page = 1;

  if (typeof req.query.page === "string") {
    const intPage = parseInt(req.query.page, 10);
    if (!isNaN(intPage)) {
      page = intPage;
    }
  }

  return page;
};

const sliceItems = (items: User[], page: number) => {
  const start = (page - 1) * ITEMS_PER_PAGE;
  const end = start + ITEMS_PER_PAGE;

  return items.slice(start, end);
};

const willFail = () => {
  return Math.random() < FAILURE_RATE;
};
