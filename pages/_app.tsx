import { AppProps } from "next/app";
import React from "react";
import { InfiniteScrollStateProvider } from "../contexts/infinite-scroll";

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <InfiniteScrollStateProvider>
      <Component {...pageProps} />
    </InfiniteScrollStateProvider>
  );
};

export default App;
