import { NextPage } from "next";
import Image from "next/image";
import NextLink from "next/link";
import React from "react";
import { Links } from "../components/Links";
import { INFINITE_SCROLL_KEY } from "../constants/infinite-scroll";
import { useInfiniteScroll } from "../hooks/infinite-scroll";
import { User } from "../model/user";

const buildApiPath = (page: number) => {
  if (process.env.NODE_ENV === "production") {
    return `https://infinite-scroll-dun.vercel.app/api/users?page=${page}`;
  } else {
    return `http://localhost:3000/api/users?page=${page}`;
  }
};

const Page: NextPage = () => {
  /**
   * timerId, errorMessage, setErrorMessage
   *
   * Infinite Scroll とは無関係。
   * 読み込みに失敗した際のエラーメッセージの表示非表示に使う。
   */
  const timerId = React.useRef<NodeJS.Timeout>();
  const [errorMessage, setErrorMessage] = React.useState<string>();

  /**
   * infiniteScroll
   *
   * Infinite Scroll を実現するためのカスタムフック useInfiniteScroll の返り値
   */
  const infiniteScroll = useInfiniteScroll({
    key: INFINITE_SCROLL_KEY.INDEX,
    intersectionObserverOptions: {
      rootMargin: "320px",
    },
    buildApiPath,
    onLoad: () => {
      setErrorMessage(undefined);
    },
    onError: (error: Error) => {
      if (timerId.current != null) {
        clearTimeout(timerId.current);
      }
      setErrorMessage(error.message);
      timerId.current = setTimeout(() => {
        setErrorMessage(undefined);
      }, 2000);
    },
  });

  return (
    <>
      <Links h1="Index" />
      <ul>
        {/* TODO: as を使わない形へ修正する */}
        {(infiniteScroll.items as User[]).map((item, index) => {
          const href = `/users/${item.id}`;
          return (
            <li key={`${item.id}-${index}`}>
              <NextLink href={href}>
                <a>
                  <PageCard user={item} />
                </a>
              </NextLink>
            </li>
          );
        })}
      </ul>

      <div ref={infiniteScroll.sentinelRef}></div>

      {/* 要素を追加する際には、infiniteScroll.loading を用いて、要素を追加するトリガーとなった要素は描画されないようにする。 */}
      {/* これをしないと、スクロールアンカリングの効果によって、トリガーとなった要素が画面内に表示されるようにスクロール位置がコントロールされ、 */}
      {/* ユーザーが追加された要素を認識できなくなってしまう。 */}
      {/* CSS の overflow-anchor: none; でも制御可能。 */}
      <div className="load-more">
        {!infiniteScroll.finished ? (
          infiniteScroll.loading ? (
            <span>読み込み中...</span>
          ) : (
            <button onClick={infiniteScroll.load}>さらに読み込む</button>
          )
        ) : (
          <span>すべての要素を読み込みました。</span>
        )}
      </div>

      {errorMessage != null ? (
        <div className="error">{errorMessage}</div>
      ) : null}

      <style jsx>{`
        ul {
          list-style: none;
          margin: 0;
          padding: 32px;
        }
        li {
          margin-top: 8px;
        }
        a {
          color: inherit;
          text-decoration: inherit;
        }
        .load-more {
          display: grid;
          place-items: center;
          padding-bottom: 32px;
        }
        .error {
          position: fixed;
          right: 0;
          bottom: 0;
          padding: 4px;
          font-size: 12px;
          background: crimson;
          color: white;
        }
      `}</style>
    </>
  );
};

export default Page;

const PageCard: React.FC<{ user: User }> = (props) => {
  return (
    <>
      <div className="card">
        <div className="card__index">{props.user.index}</div>
        <div className="card__image">
          <Image
            width="100"
            height="100"
            src={`https://robohash.org/${props.user.id}.png?set=set4`}
            alt="アバター画像"
          />
        </div>
        <div className="card__title">{props.user.name}</div>
        <div className="card__body">{props.user.message}</div>
      </div>

      <style jsx>{`
        .card {
          padding: 16px;
          display: grid;
          grid-template: auto auto auto / auto 48px 1fr;
          gap: 8px 16px;
          background: ghostwhite;
        }
        .card__index {
          grid-area: 1 / 1 / 4 / 2;
          font-size: 16px;
          font-weight: bold;
        }
        .card__image {
          grid-area: 1 / 2 / 3 / 3;
          width: 48px;
          height: 48px;
          border-radius: 50%;
          background: white;
          overflow: hidden;
        }
        .card__title {
          grid-area: 1 / 3 / 2 / 4;
          font-size: 20px;
          font-weight: bold;
        }
        .card__body {
          grid-area: 2 / 3 / 3 / 4;
          font-size: 16px;
        }
      `}</style>
    </>
  );
};
