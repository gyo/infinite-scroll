/**
 * INFINITE_SCROLL_KEY
 *
 * 各 Infinite Scroll ごとの固有の値。
 *
 * ページを遷移しても Infinite Scroll の一覧やスクロール位置を保存するため、React Context を利用している。
 * この React Context はアプリケーションでグローバルなので、Infinite Scroll ごとにキーを設定する必要がある。
 */
export const INFINITE_SCROLL_KEY = {
  INDEX: "index",
  USERS: "users",
  MEMBERS: "members",
} as const;

export type InfiniteScrollKeys =
  typeof INFINITE_SCROLL_KEY[keyof typeof INFINITE_SCROLL_KEY];
