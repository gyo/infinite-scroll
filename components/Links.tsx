import NextLink from "next/link";
import React from "react";
import { useRouter } from "next/router";

type Props = {
  h1: string;
};

export const Links: React.FC<Props> = (props) => {
  const router = useRouter();

  return (
    <>
      <div>
        <h1>{props.h1}</h1>
        <ul>
          <li>
            <button onClick={() => router.back()}>戻る</button>
          </li>
          <li>
            <NextLink href={`/`}>
              <a>Index</a>
            </NextLink>
          </li>
          <li>
            <NextLink href={`/users`}>
              <a>Users</a>
            </NextLink>
          </li>
          <li>
            <NextLink href={`/members`}>
              <a>Members</a>
            </NextLink>
          </li>
        </ul>
      </div>

      <style jsx>{`
        div {
          position: fixed;
          z-index: 1;
          top: 0;
          left: 0;
          padding: 8px;
          width: 100%;
          background: white;
        }
        ul {
          margin: 0;
          padding: 0;
          display: flex;
          gap: 8px;
          list-style: none;
        }
        h1 {
          margin: 0;
          font-size: 14px;
        }
        a {
          display: block;
          font-size: 14px;
        }
      `}</style>
    </>
  );
};
