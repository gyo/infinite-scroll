export type User = {
  index: number;
  id: string;
  name: string;
  message: string;
};
