export type Member = {
  index: number;
  id: string;
  name: string;
  tel: string;
  email: string;
};
