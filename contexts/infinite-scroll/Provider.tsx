import React from "react";
import {
  infiniteScrollStateContext,
  setInfiniteScrollStateContext,
} from "./context";
import { infiniteScrollInitialStore, InfiniteScrollStore } from "./store";

/**
 * InfiniteScrollStateProvider
 *
 * Infinite Scroll に関連するグローバルな React Context の Provider コンポーネント。
 * _app.tsx で利用する。
 */
export const InfiniteScrollStateProvider: React.FC = (props) => {
  const [state, setState] = React.useState<InfiniteScrollStore>(
    infiniteScrollInitialStore
  );

  return (
    <infiniteScrollStateContext.Provider value={state}>
      <setInfiniteScrollStateContext.Provider value={setState}>
        {props.children}
      </setInfiniteScrollStateContext.Provider>
    </infiniteScrollStateContext.Provider>
  );
};
