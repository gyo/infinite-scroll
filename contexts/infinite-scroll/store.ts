import { INFINITE_SCROLL_KEY } from "../../constants/infinite-scroll";
import { Member } from "../../model/member";
import { User } from "../../model/user";

export type InfiniteScrollState<T = never> = {
  items: T[];
  page: number;
  scrollY: number;
};

export type InfiniteScrollStore = {
  [INFINITE_SCROLL_KEY.INDEX]: InfiniteScrollState<User>;
  [INFINITE_SCROLL_KEY.USERS]: InfiniteScrollState<User>;
  [INFINITE_SCROLL_KEY.MEMBERS]: InfiniteScrollState<Member>;
};

/**
 * infiniteScrollDummyStore
 *
 * React.createContext の際の型エラーを防ぐための仮の値
 * 実際には利用されない
 */
export const infiniteScrollDummyStore: InfiniteScrollStore = {
  [INFINITE_SCROLL_KEY.INDEX]: {
    items: [],
    page: 0,
    scrollY: 0,
  },
  [INFINITE_SCROLL_KEY.USERS]: {
    items: [],
    page: 0,
    scrollY: 0,
  },
  [INFINITE_SCROLL_KEY.MEMBERS]: {
    items: [],
    page: 0,
    scrollY: 0,
  },
};

export const infiniteScrollInitialStore: InfiniteScrollStore = {
  [INFINITE_SCROLL_KEY.INDEX]: {
    items: [],
    page: 0,
    scrollY: 0,
  },
  [INFINITE_SCROLL_KEY.USERS]: {
    items: [],
    page: 0,
    scrollY: 0,
  },
  [INFINITE_SCROLL_KEY.MEMBERS]: {
    items: [],
    page: 0,
    scrollY: 0,
  },
};
