import React from "react";
import { infiniteScrollDummyStore, InfiniteScrollStore } from "./store";

/**
 * infiniteScrollStateContext
 *
 * Infinite Scroll に関する状態を保持するための React Context
 * ※Provider を作成する際に React.useState の返り値を初期値として設定する
 */
export const infiniteScrollStateContext = React.createContext<
  InfiniteScrollStore
>(infiniteScrollDummyStore);

/**
 * setInfiniteScrollStateContext
 *
 * Infinite Scroll に関する状態を更新するための React Context
 * ※Provider を作成する際に React.useState の返り値を初期値として設定する
 */
export const setInfiniteScrollStateContext = React.createContext<
  React.Dispatch<React.SetStateAction<InfiniteScrollStore>>
>(() => {
  throw new Error("setStateContext is not implemented.");
});
