import React from "react";
import { InfiniteScrollKeys } from "../../constants/infinite-scroll";
import {
  infiniteScrollStateContext,
  setInfiniteScrollStateContext,
} from "./context";
import { InfiniteScrollStore } from "./store";

type UseInfiniteScrollContext = (
  key: InfiniteScrollKeys
) => [
  InfiniteScrollStore[InfiniteScrollKeys],
  (partial: Partial<InfiniteScrollStore[InfiniteScrollKeys]>) => void
];

/**
 * useInfiniteScrollContext
 *
 * Infinite Scroll に関連するグローバルな React Context の値と、
 * その値を更新する関数を取得するカスタムフック。
 * hooks/infinite-scroll.ts で利用する。
 */
export const useInfiniteScrollContext: UseInfiniteScrollContext = (key) => {
  const state = React.useContext(infiniteScrollStateContext);
  const setState = React.useContext(setInfiniteScrollStateContext);

  const keyState = React.useMemo(() => {
    return state[key];
  }, [key, state]);

  const setKeyState = React.useCallback(
    (newKeyState: Partial<InfiniteScrollStore[typeof key]>) => {
      setState((state) => {
        return {
          ...state,
          [key]: {
            ...state[key],
            ...newKeyState,
          },
        };
      });
    },
    [key, setState]
  );

  return [keyState, setKeyState];
};
